// Import Express Library
const express = require("express");

// Declare Router
const router = express.Router();

// Import Course Middleware
const courseMiddleware = require("../middlewares/courseMiddleware");
//Get Method
router.get("/courses", courseMiddleware.getAllCoursesMiddleware, (request, response) => {
  response.json({
    Mess: "Get all courses",
  });
  console.log("Get all courses Router");
});

//Post Method
router.post("/courses", courseMiddleware.createNewCourseMiddleware, (request, response) => {
  response.json({
    Mess: "Post New Course",
  });
  console.log("Post New Course Router");
});

//Put Method
router.put("/courses/:courseId", courseMiddleware.updateCourseMiddleware, (request, response) => {
  const courseId = request.params.courseId;
  response.json({
    Mess: `Course ${courseId} has updated`,
  });
  console.log(`Course ${courseId} has updated Router`);
});

//Delete Method
router.delete("/courses/:courseId", courseMiddleware.deleteCourseMiddleware, (request, response) => {
  const courseId = request.params.courseId;
  response.json({
    Mess: `Course ${courseId} has deleted`,
  });
  console.log(`Course ${courseId} has deleted Router`);
});

//Export Router
module.exports = router;
