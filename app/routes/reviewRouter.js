//Import express Lib
const express = require("express");
const router = express.Router();
//Get Method
router.get("/reviews", (request, response) => {
  response.json({
    Message: "Get all Reviews",
  });
});

//Post Method
router.post("/reviews", (request, response) => {
  response.json({
    Message: "Post new Review",
  });
});

//Put Method
router.put("/reviews/:reviewId", (request, response) => {
  const reviewId = request.params.reviewId;
  response.json({
    Message: `${reviewId} has updated`,
  });
});

//Delete Method
router.delete("/reviews/:reviewId", (request, response) => {
  const reviewId = request.params.reviewId;
  response.json({
    Message: `${reviewId} has deleted`,
  });
});

//Export
module.exports = router;
