const { request, response } = require("express");

//Get all course middleware
const getAllCoursesMiddleware = (request, response, next) => {
  console.log("Get all courses middleware");
  next();
};

//Create new course middleware
const createNewCourseMiddleware = (request, response, next) => {
  console.log("Create new course middleware");
  next();
};

//Update course middleware
const updateCourseMiddleware = (request, response, next) => {
  console.log("Update Course Middleware");
  next();
};

//Delete course middleware
const deleteCourseMiddleware = (request, response, next) => {
  console.log("Delete Course Middleware");
  next();
};

//Export
module.exports = {
  getAllCoursesMiddleware,
  createNewCourseMiddleware,
  updateCourseMiddleware,
  deleteCourseMiddleware,
};
