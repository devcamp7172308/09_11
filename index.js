//Express library
const express = require("express");
// Declare app
const app = express();
// Declare port
const port = 8000;

//Import Course Router
const courseRouter = require("./app/routes/courseRouter");

//Import Review Router
const reviewRouter = require("./app/routes/reviewRouter");
//FIRST: Middleware
app.use((request, response, next) => {
  console.log("Hello middleware");
  next();
});

//SECOND Get method
app.get("/", (request, response) => {
  console.log("Get method");
  response.json({
    message: "Devcamp Middleware App",
  });
});

//THIRD App using COURSE ROUTERS
app.use("/api", courseRouter);

//FOURTH App using REVIEW ROUTERS
app.use("/api", reviewRouter);

//Run app on port
app.listen(port, () => {
  console.log(`Apps running on port:`, port);
});
